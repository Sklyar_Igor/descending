package com.company;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;


public class Main {
    public static void main(String[] args) throws Exception {
        //check if source file exists
        File number = new File("Numbers.txt");
        File sorted = new File("Sorted.txt");
        if (!number.exists()) {
            try ( // create the file
                  PrintWriter output = new PrintWriter(number);
            ) {
                for (int i = 0; i <= 20; i++) {
                    output.print(((int) (Math.random() * 20)) + " ");
                }
            }
        }
        try (
                Scanner input = new Scanner(number);
        ) {
            Integer[] numbers = new Integer[20];
            for (int i = 0; i < 20; i++) {

                numbers[i] = input.nextInt();
              //  System.out.print(numbers[i] + ",");

            }
            Arrays.sort(numbers, Collections.reverseOrder());
            for (int i = 0; i < 20; i++) {
                System.out.print(numbers[i] + ",");
            }
        }
    }
}